﻿using UnityEngine;
using System.Collections;

public static class GameObjectExtension  {
	public static GameObject FindChild(this GameObject gameobject,string name)
	{
		foreach (Transform t in gameobject.GetComponentsInChildren<Transform>()) {
			if (t.name.CompareTo (name) == 0) {
				return t.gameObject;
			}
		}
		return null;
	}
	public static void SetParentClearPosition(this GameObject go,Transform parent)
	{
		go.transform.SetParent (parent);
		go.transform.localPosition = Vector3.zero;
		go.transform.localRotation = Quaternion.identity;
	}
	public static void LookAtXZ(this Transform transform,Vector3 pos)
	{
		pos.y = transform.position.y;
		transform.LookAt (pos);
	}
	public static void MoveTo(this GameObject go,Vector3 destiny,float speed=4.0f){
		go.transform.position += (destiny - go.transform.position).normalized * Time.deltaTime * speed;

	}
	public static bool reach(this Transform go, Vector3 destiny,float distance){
		return Vector3.Distance (destiny, go.position) <distance;
	}
   
    

}


