﻿using UnityEngine;
using System.Collections;
using System;
public class MouseEventArgs:EventArgs{
	public MouseEventArgs (Vector2 mousePosition)
	{
		this.mousePosition = mousePosition;
		this.dragDelta = Vector2.zero;
		this.dragSpeed = Vector2.zero;
	}
	public MouseEventArgs (Vector2 mousePosition, Vector2 dragDelta,Vector2 dragSpeed,Vector2 dragMovement)
	{
		this.mousePosition = mousePosition;
		this.dragDelta = dragDelta;
		this.dragSpeed = dragSpeed;
		this.dragMovement = dragMovement;
	}
   


    public Vector2 MousePosition {
		get {
			return this.mousePosition;
		}
	}

	public Vector2 DragDelta {
		get {
			return this.dragDelta;
		}
	}
	public Vector2 DragSpeed {
		get {
			return this.dragSpeed;
		}
	}
	public Vector2 DragMovement {
		get {
			return this.dragMovement;
		}
	}
	Vector2 mousePosition;
	Vector2 dragDelta;
	Vector2 dragSpeed;
	Vector2 dragMovement;

}
public class InputHandler : GlobalSystem<InputHandler> {
	public EventHandler<MouseEventArgs> OnClick;
	public EventHandler<MouseEventArgs> OnMouseDown;
	public EventHandler<MouseEventArgs> OnMouseUp;
	public EventHandler<MouseEventArgs> OnMouseDrag;
	public EventHandler<MouseEventArgs> OnMouseDrop;
	public EventHandler<MouseEventArgs> OnSwipe;
	[SerializeField] float MouseDragThreshold=3;
    [SerializeField] float MouseSwipeDistanceThreshold= 30;
    [SerializeField] float MouseSwipeSpeedThreshold = 30;
    [SerializeField] float MouseClickThreshold=1;

	Vector2 mouseDownPosition=Vector2.zero;
	Vector2 mouseLastPosition=Vector2.zero;
	Vector2 mouseMovementDeltaFixed=Vector2.zero;
	Vector2 mouseMovementDelta=Vector2.zero;
	Vector2 mouseMovementDistance=Vector2.zero;
    float mouseSwipeDistance= 0;
    float mouseMovementSpeedCount=0.0f;
	Vector2 mouseMovementSpeedSum=Vector2.zero;
	Vector2 mouseMovementSpeed=Vector2.zero;
	[SerializeField]float dragSleepTime=0.3f;
	[SerializeField]float dragSleepSpeed=0.03f;
	float idleTime=0.0f;
	float mouseDownTime=0;
	bool dragging=false;
	// Use this for initialization
	void Start () {
		
	}
    void fireOnMouseDrag(MouseEventArgs args)
    {
        if (OnMouseDrag != null)
        {
            OnMouseDrag(this, args);
        }
    }
    void fireOnMouseSwipe(MouseEventArgs args)
    {
            if (OnSwipe != null)
            {
            OnSwipe(this, args);
            }
    }
	void fireOnMouseUp(MouseEventArgs args){
		if (OnMouseUp!=null) {
			OnMouseUp (this,args);
		}	
	}
	void fireOnMouseDown(MouseEventArgs args){
		if (OnMouseDown!=null) {
			OnMouseDown (this,args);
		}	
	}
	void fireOnMouseDrop(MouseEventArgs args){
		if (OnMouseDrop!=null) {
			OnMouseDrop (this,args);
		}	
	}
	void fireOnClick(MouseEventArgs args){
		if (OnClick!=null) {
			OnClick (this,args);
		}	

	}
	// Update is called once per frame
	void Update () {
		mouseMovementDelta = ((Vector2)Input.mousePosition) - mouseLastPosition;
		mouseMovementDeltaFixed = new Vector2(mouseMovementDelta.x/Screen.width,mouseMovementDelta.y/Screen.height);
		Vector2 deltaSpeed = mouseMovementDeltaFixed / Time.unscaledDeltaTime;
		if (deltaSpeed.magnitude < dragSleepSpeed) {
			idleTime += Time.unscaledDeltaTime;
			if (idleTime > dragSleepTime) {
				mouseMovementSpeedCount=0.0f;
				mouseMovementSpeedSum  =Vector2.zero;
				mouseMovementDistance = Vector2.zero;
			}
		} else {
			idleTime = 0.0f;
			mouseMovementSpeedCount+=1.0f;
			mouseMovementSpeedSum = mouseMovementSpeedSum+ deltaSpeed;
			mouseMovementSpeed = mouseMovementSpeedSum *(1/mouseMovementSpeedCount);
			mouseMovementDistance += mouseMovementDeltaFixed;
		}
        if (deltaSpeed.magnitude < MouseSwipeSpeedThreshold)
        {
            mouseSwipeDistance = 0;
        }
        else
        {
            mouseSwipeDistance += mouseMovementDelta.magnitude;

        }



            if (Input.GetMouseButtonDown (0)) {
			mouseDownPosition = Input.mousePosition;
			mouseDownTime = 0;
            mouseSwipeDistance = 0;
			mouseMovementSpeedCount=0;
			mouseMovementSpeedSum = Vector2.zero;
			mouseMovementDistance = Vector2.zero;
			fireOnMouseDown (new MouseEventArgs (mouseDownPosition));
		}
		if (Input.GetMouseButtonUp (0)) {
			fireOnMouseUp (new MouseEventArgs (Input.mousePosition));
			if (mouseDownTime < MouseClickThreshold) {
				fireOnClick (new MouseEventArgs(Input.mousePosition));				
			}
			if (dragging)	
				fireOnMouseDrop (new MouseEventArgs (Input.mousePosition, mouseMovementDelta,mouseMovementSpeed,mouseMovementDistance));
			dragging = false;
		}
		if (Input.GetMouseButton (0)) {
			mouseDownTime += Time.unscaledDeltaTime;
			if ((((Vector2)Input.mousePosition) - mouseDownPosition).magnitude>MouseDragThreshold) {
				dragging = true;
			}
            if ((((Vector2)Input.mousePosition) - mouseDownPosition).magnitude > MouseDragThreshold)
            {
                dragging = true;
            }
            if (mouseSwipeDistance>MouseSwipeDistanceThreshold)
            {
                fireOnMouseSwipe(new MouseEventArgs(Input.mousePosition, mouseMovementDelta, mouseMovementSpeed, mouseMovementDistance));
                
            }
            
            if (dragging) fireOnMouseDrag (new MouseEventArgs (Input.mousePosition, mouseMovementDelta,mouseMovementSpeed,mouseMovementDistance));
		}
		mouseLastPosition = Input.mousePosition;
	}
}
