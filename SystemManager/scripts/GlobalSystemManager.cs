﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalSystemManager : AsyncInitializer
{
	[SerializeField]List<GameObject> prefabs;
	[SerializeField]List<GameObject> systems;

	private static GlobalSystemManager instance = null;

	public static GlobalSystemManager Instance {
		get {
			return instance;
		}
	}

	public static bool Exists {
		get {
			return instance != null;
		}
	}

	void Awake ()
	{
		
		loaded = false;
		if (instance != null) {
			Destroy (gameObject);
		} else {
			DontDestroyOnLoad (gameObject);
			instance = this;
			foreach (GameObject p in prefabs) {
				GameObject go = GameObject.Instantiate (p);
				systems.Add (go);
				go.transform.parent = transform;
			}
		}
	}

	public static T  getSystem<T> () where T:class
	{
		foreach (GameObject go in Instance.systems) {
			T sistema = go.GetComponentInChildren<T> ();
			if (sistema != null)
				return sistema;
		}
		return null;
	}
	// Update is called once per frame
	void Update ()
	{
		if (!loaded) {
			bool loadedAll = true;
			foreach (GameObject go in systems) {
				AsyncInitializer initializer= go.GetComponentInChildren<AsyncInitializer> ();
				if (initializer != null && !initializer.isLoaded ()) {
					loadedAll = false;
					break;
				}
			}
			loaded = loadedAll;
		}
	}
}
