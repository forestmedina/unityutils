﻿using UnityEngine;
using System.Collections;

public class AsyncInitializer : MonoBehaviour {
	protected bool loaded=false;
	public virtual bool isLoaded(){
		return loaded;
	}

}
