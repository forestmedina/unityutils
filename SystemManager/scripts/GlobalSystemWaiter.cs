﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalSystemWaiter : MonoBehaviour {
	[SerializeField] List<GameObject> activate;
	[SerializeField] float  minTime=0.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if (minTime<0.0f&&GlobalSystemManager.Instance.isLoaded ()) {
			foreach(GameObject go in activate){
				go.SetActive (true);
			}
			gameObject.SetActive( false);
		}
		minTime -= Time.deltaTime;
	}
}
