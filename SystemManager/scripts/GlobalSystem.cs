﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSystem<T> :MonoBehaviour where T : class
{

    public static T Instance{
        get {
            return GlobalSystemManager.getSystem<T>();
        }
     }
}
