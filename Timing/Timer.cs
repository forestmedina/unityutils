﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Timer  {

    public System.EventHandler<System.EventArgs> OnTimer;

    float time;
    bool  notified=false;
    public bool debug = false;
    public void count(float time)
    {
        notified = false;
        this.time = time;
    }
    public bool finished
    {
        get { return time <= 0; }
    }
    public void update(float dt)
    {

        if (time > 0)
        {
            time -= dt;
        }
        if(time<=0&&!notified){
            notified = true;
            if (debug) Debug.Log("Notifying");
            if (OnTimer != null)
            {
                if (debug ) Debug.Log("Invoking");
                OnTimer(this, System.EventArgs.Empty);
            }

        }
        
    }
}
