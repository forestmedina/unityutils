﻿using UnityEngine;
using System.Collections;

public class ProgressBar : MonoBehaviour {
	[SerializeField]UnityEngine.UI.Image background;
	[SerializeField]UnityEngine.UI.Image foreground;
    [SerializeField] UnityEngine.UI.Text txtValue;
    [SerializeField]  float initialValue;
	float currentValue;
	float speed;
	bool updating=true;
    public int referenceValue=100;

    // Use this for initialization
    void Start () {
		foreground.fillAmount =0;
	}
	public void setValue(float valor,float time=0.5f){

        setValueBySpeed(valor, time == 0 ? 0: (currentValue - foreground.fillAmount) / time);
	}
    public void setValueBySpeed(float valor, float speed = 0.2f)

    {
        if (speed == 0) foreground.fillAmount = valor;
        if (valor != currentValue)
        {
            currentValue = Mathf.Clamp(valor, 0, 1);
            this.speed=speed;
            
            updating = true;
        }
    }
    public float AnimatedValue
    {
        get { return foreground.fillAmount; }
    }
	// Update is called once per frame
	void Update () {
		if (!updating)
			return;
		if((speed<0&&(currentValue-foreground.fillAmount)>0)||(speed>0&&(currentValue-foreground.fillAmount)<0||speed==0)){
			foreground.fillAmount = currentValue;
			updating = false;
		}else{
			foreground.fillAmount += speed*Time.deltaTime;
		}
        if(txtValue!=null)
            txtValue.text = (foreground.fillAmount * (float)referenceValue).ToString("###0") + "/" + referenceValue.ToString();
        


	}
}
