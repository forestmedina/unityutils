﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscreteProgressBar : MonoBehaviour {

    int value=0;
    List<DiscreteBarItem> items = new List<DiscreteBarItem>();
	// Use this for initialization
	void Start () {
        items.AddRange(GetComponentsInChildren<DiscreteBarItem>());
	}
	public int Value
    {
        get { return this.value; }
        set{
            if (this.value != value)
            {
                this.value = value;
                UpdateBar();
            }
        }
    }
    private void UpdateBar()
    {
        int i = 0;
        foreach(DiscreteBarItem item in items)
        {
            item.SetActive(i + 1<= value);
            i++;
        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
