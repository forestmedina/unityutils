﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DiscreteBarItem : MonoBehaviour {
    [SerializeField] Image foreGround;
	public void SetActive(bool active)
    {
        if(foreGround!=null)
        foreGround.gameObject.SetActive(active);
    }
    public bool Active
    {
        get { return foreGround!=null&&foreGround.gameObject.activeSelf; }
    }
}
