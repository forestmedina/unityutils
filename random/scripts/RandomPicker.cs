﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System;

public class RandomPicker {
	public static T Pick<T>(IEnumerable<T> enumerable){
		List<T> list=new List<T>(enumerable);
		if (list.Count==0)
			return default(T);
		
		return list[UnityEngine.Random.Range (0, list.Count )];
		
	}
}
