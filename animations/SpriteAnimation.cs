﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour {
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] UnityEngine.UI.Image image;
    [SerializeField]List<Sprite> sprites;
    [SerializeField] bool playOnStart;
    [SerializeField] bool loop;
    [SerializeField] bool useUnscaledTime=false;
    IEnumerator<Sprite> spritesIEnumerator;
    [SerializeField]float delay=0.04f;
    float timeToNextFrame = 0;
    // Use this for initialization
    void Start () {
        spritesIEnumerator = sprites.GetEnumerator();
        if (playOnStart) Play();
	}
    public void Play()
    {
        timeToNextFrame = 0;
        if(spritesIEnumerator==null)
            spritesIEnumerator = sprites.GetEnumerator();
        spritesIEnumerator.Reset();
    }
	
	// Update is called once per frame
	void Update () {
        while (timeToNextFrame < 0)
        {
            timeToNextFrame += delay;
            if (!spritesIEnumerator.MoveNext())
            {
                if (loop) spritesIEnumerator.Reset();
            }
            if (spriteRenderer != null)
                spriteRenderer.sprite = spritesIEnumerator.Current;
            if (image != null) { 
                image.sprite = spritesIEnumerator.Current;
            }
            
        }
        timeToNextFrame -= useUnscaledTime?Time.unscaledDeltaTime:Time.deltaTime;
	}
}
