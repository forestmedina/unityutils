﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class TweeningMove : TweeningAction {
	[SerializeField]Vector3 finalPos;
	[SerializeField]Vector3 initialPos;
	[SerializeField]private bool _finished=false;
	[SerializeField]public readonly float duration;
	[SerializeField]public float elapsedTime;
	private Transform _transform;

	public TweeningMove (Vector2 position,float duration){
		this.finalPos = position;
		this.duration = duration;
	}

	public override void Update ()
	{
		if (_finished)
			return;
		base.Update ();
		elapsedTime += Time.deltaTime;
		var newPos=  Vector3.Lerp(initialPos,finalPos, elapsedTime / duration);
		_transform.position = newPos;
		if (elapsedTime > duration) {
			_finished = true;
		}
	}

	public override bool Finished ()
	{
		return _finished;

	}
	public override void Start (GameObject go)
	{
		
		base.Start (go);

		_finished = false;
		initialPos = go.transform.position;
		_transform = go.transform;
		elapsedTime = 0;
		
	}

}
