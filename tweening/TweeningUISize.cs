﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class TweeningUISize : TweeningAction {
	[SerializeField]Vector2 finalSize;
	[SerializeField]Vector2 initialSize;
	[SerializeField]bool _finished=true;
	[SerializeField]public float duration;
	[SerializeField]public float elapsedTime;
	RectTransform rectTransform;

	public TweeningUISize (Vector2 size,float duration){
		this.finalSize = size;
		this.duration = duration;
	}

	public override void Update ()
	{
		if (_finished)
			return;
		base.Update ();
		elapsedTime += Time.deltaTime;
		Vector2 newSize=  Vector2.Lerp(initialSize,finalSize, elapsedTime / duration);
	
		rectTransform.sizeDelta = newSize;
		if (elapsedTime > duration) {
			_finished = true;
		}
	}

	public override bool Finished ()
	{
		return _finished;

	}
	public override void Start (GameObject go)
	{
		
		base.Start (go);

		rectTransform= go.GetComponent<RectTransform> ();
		if (rectTransform != null) {
			_finished = false;
			initialSize = rectTransform.sizeDelta;
			elapsedTime = 0;
		} else {
			_finished = true;
		}
	}

}
