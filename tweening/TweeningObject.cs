﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TweeningObject :MonoBehaviour {

	TweeningAction currentAction=null;
	[SerializeField] List<TweeningAction> actions = new List<TweeningAction> ();

	private bool repeat = false;
	// Use this for initialization
	void Start () {
		
	}
	public bool isTweening{
		get{
			return actions.Count > 0;
		}
	}
	public void Execute (TweeningAction action,bool repeat=false){
		actions.Clear ();
		actions.Add (action);
		this.repeat = repeat;
		NextAction ();
	}
	public void Queue(TweeningAction action){
		actions.Add (action);
	}
	private void NextAction(){
		if (actions.Count > 0) {
			currentAction = actions [0];
			currentAction.Start (gameObject);
		} else {
			currentAction = null;
		}
	}

	// Update is called once per frame
	void Update () {
		if(currentAction!=null) {
			currentAction.Update ();
			if(currentAction.Finished()){
				actions.RemoveAt (0);
				if ( repeat ) actions.Add(currentAction);
				NextAction ();

			}
		}
	}
}
