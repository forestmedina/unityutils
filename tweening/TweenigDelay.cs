﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenigDelay : TweeningAction {
	float secondsToDelay;
	[SerializeField]bool _finished=true;
	public TweenigDelay (float secondsToDelay)
	{
		this.secondsToDelay = secondsToDelay;
	}
	float currentTime = 0;
	// Use this for initialization
	public override void Update ()
	{
		if (_finished)
			return;
		base.Update ();
		currentTime += Time.deltaTime;
		if (currentTime> secondsToDelay) {
			_finished = true;
		}
	}

	public override bool Finished ()
	{
		return _finished;

	}
	public override void Start (GameObject go)
	{

		base.Start (go);


			_finished = false;
			currentTime= 0;

	}

}
