﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweeningExecute:TweeningAction {
	float secondsToDelay;
	[SerializeField]bool _finished=true;
	System.Action callback;
	public TweeningExecute (System.Action callback  )
	{
		this.callback=callback;
	}
	// Use this for initialization
	public override void Update ()
	{
		if (_finished)
			return;
		base.Update ();
		_finished = true;
		callback ();
	}

	public override bool Finished ()
	{
		return _finished;

	}
	public override void Start (GameObject go)
	{

		base.Start (go);
		_finished = false;

	}

}
