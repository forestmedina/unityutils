﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweeningUIRotate : TweeningAction {
	[SerializeField]Quaternion finalRotation;
	[SerializeField]Quaternion initialRotation;
	[SerializeField]bool _finished=true;
	[SerializeField]public float duration;
	[SerializeField]public float elapsedTime;
	RectTransform rectTransform;

	public TweeningUIRotate (Quaternion size,float duration){
		this.finalRotation = size;
		this.duration = duration;
	}

	public override void Update ()
	{
		if (_finished)
			return;
		base.Update ();
		elapsedTime += Time.deltaTime;
		Quaternion newRotation=  Quaternion.Lerp(initialRotation,finalRotation, elapsedTime / duration);

		rectTransform.rotation = newRotation;
		if (elapsedTime > duration) {
			_finished = true;
		}
	}

	public override bool Finished ()
	{
		return _finished;

	}
	public override void Start (GameObject go)
	{

		base.Start (go);

		rectTransform= go.GetComponent<RectTransform> ();
		if (rectTransform != null) {
			_finished = false;
			initialRotation = rectTransform.rotation;
			elapsedTime = 0;
		} else {
			_finished = true;
		}
	}
}
