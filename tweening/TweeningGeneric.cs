﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweeningGeneric  : TweeningAction {
	[SerializeField]Vector3 finalPos;
	[SerializeField]Vector3 initialPos;
	[SerializeField]private bool _finished=false;
	[SerializeField]private readonly float duration;
	private float elapsedTime;
	[SerializeField] private readonly Action<float> _tweeningFunc;
	private Transform _transform;

	public TweeningGeneric (Action<float> tweeningFunc,float duration)
	{
		this._tweeningFunc = tweeningFunc;
		this.duration = duration;
	}

	public override void Update ()
	{
		if (_finished)
			return;
		base.Update ();
		elapsedTime += Time.deltaTime;
		var interpolation = elapsedTime / duration;
		_tweeningFunc(interpolation);
		if (elapsedTime > duration) {
			_finished = true;
		}
	}

	public override bool Finished ()
	{
		return _finished;

	}
	public override void Start (GameObject go)
	{
		
		base.Start (go);

		_finished = false;
		initialPos = go.transform.position;
		_transform = go.transform;
		elapsedTime = 0;
		
	}

}
