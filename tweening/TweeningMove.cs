﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class TweeningUIMove : TweeningAction {
	[SerializeField]Vector2 finalPos;
	[SerializeField]Vector2 initialPos;
	[SerializeField]bool _finished=true;
	[SerializeField]public float duration;
	[SerializeField]public float elapsedTime;
	RectTransform rectTransform;

	public TweeningUIMove (Vector2 position,float duration){
		this.finalPos = position;
		this.duration = duration;
	}

	public override void Update ()
	{
		if (_finished)
			return;
		base.Update ();
		elapsedTime += Time.deltaTime;
		Vector2 newPos=  Vector2.Lerp(initialPos,finalPos, elapsedTime / duration);
		rectTransform.anchoredPosition = newPos;
		if (elapsedTime > duration) {
			_finished = true;
		}
	}

	public override bool Finished ()
	{
		return _finished;

	}
	public override void Start (GameObject go)
	{
		
		base.Start (go);

		rectTransform= go.GetComponent<RectTransform> ();
		if (rectTransform != null) {
			_finished = false;
			initialPos = rectTransform.anchoredPosition;
			elapsedTime = 0;
		} else {
			_finished = true;
		}
	}

}
