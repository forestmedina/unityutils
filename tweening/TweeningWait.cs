﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweeningWait : TweeningAction {
	[SerializeField]bool _finished=true;
	[SerializeField]public float duration;
	[SerializeField]public float elapsedTime;
	GameObject go;
	bool enable;

	public TweeningWait (float duration=1){
		this.duration = duration;
	}

	public override void Update ()
	{
		if (_finished)
			return;
		base.Update ();
		elapsedTime += Time.deltaTime;
		if (elapsedTime > duration) {
			_finished = true;
		}
	}

	public override bool Finished ()
	{
		return _finished;

	}
	public override void Start (GameObject go)
	{
		base.Start (go);
		_finished = false;
		elapsedTime = 0;
	}

}
