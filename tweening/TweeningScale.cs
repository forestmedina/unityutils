﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class TweeningScale : TweeningAction {
	[SerializeField]Vector2 finalSize;
	[SerializeField]Vector2 initialSize;
	[SerializeField]bool _finished=true;
	[SerializeField]public float duration;
	[SerializeField]public float elapsedTime;
	private Transform _transform;

	public TweeningScale (Vector3 size,float duration){
		this.finalSize = size;
		this.duration = duration;
	}

	public override void Update ()
	{
		if (_finished)
			return;
		base.Update ();
		elapsedTime += Time.deltaTime;
		var newSize=  Vector3.Lerp(initialSize,finalSize, elapsedTime / duration);
	
		_transform.localScale = newSize;
		if (elapsedTime > duration) {
			_finished = true;
		}
	}

	public override bool Finished ()
	{
		return _finished;

	}
	public override void Start (GameObject go)
	{
		
		base.Start (go);

		_transform= go.transform;
		if (_transform != null) {
			_finished = false;
			initialSize = _transform.localScale;
			elapsedTime = 0;
		} else {
			_finished = true;
		}
	}

}
