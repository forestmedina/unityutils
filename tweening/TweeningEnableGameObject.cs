﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class   TweeningEnableGameObject: TweeningAction {
	[SerializeField]bool _finished=true;
	[SerializeField]public float duration;
	[SerializeField]public float elapsedTime;
	GameObject go;
	bool enable;

	public TweeningEnableGameObject (bool enable=true,float duration=0){
		this.duration = duration;
	}

	public override void Update ()
	{
		if (_finished)
			return;
		base.Update ();
		elapsedTime += Time.deltaTime;
		if (elapsedTime > duration) {
			go.SetActive (false);
			_finished = true;
		}
	}

	public override bool Finished ()
	{
		return _finished;

	}
	public override void Start (GameObject go)
	{
		this.go = go;
		base.Start (go);
		if (go!= null) {
			_finished = false;
			elapsedTime = 0;
		} else {
			_finished = true;
		}
	}

}
