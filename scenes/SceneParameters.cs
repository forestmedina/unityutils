﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
public class SceneParameters : MonoBehaviour {
	Dictionary<string,object> parameters=new Dictionary<string,object> ();




	
	
	// Update is called once per frame
	void Update () {
	}

	public bool haveParameter(string key){
		return parameters.ContainsKey (key);
	}
	public T consumeParameter<T>(string key){
		T value = (T) parameters [key] ;
		parameters.Remove (key);
		return value;
	}
	public  void pushParameter<T>(string key,T value){
		parameters [key] = value;	
	}


}
