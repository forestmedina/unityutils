﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sanuma.Steering
{
	public class SteeringManager : MonoBehaviour
	{
	
		List<Vector3> forces;
		Vector3 currentForce;
        Vector3 totalForce;
      
		void Start ()
		{
			forces = new List<Vector3> ();
			currentForce = Vector3.zero;
        }

		public void moveToDirection (Vector3 direction,float speed)
		{
			forces.Add (direction.normalized*speed);
		}
        public void moveToPoint(Vector3 point,float speed)
        {
            Vector3 dir = point - transform.position;
            forces.Add(dir.normalized*speed);
        }
        public void follow(Transform  target,float speed)
        {
            moveToPoint(target.position,speed);

        }
        public void Flee(Transform target,float speed)
        {
            moveToDirection((transform.position-target.position).normalized,speed);

        }
        void FixedUpdate ()
		{
			Vector3 totalForce = Vector3.zero;
			foreach (Vector3 force in forces) {
                totalForce += force;
			}

			if (forces.Count > 0)
				totalForce = totalForce / forces.Count;
			forces.Clear ();
			Vector3 finalForce = (totalForce) ;
			finalForce.y = 0;


			currentForce = (currentForce+ finalForce)*0.5f;
            if (currentForce.magnitude > 0.1f)
            {
                transform.LookAt(transform.position + currentForce);
            }
			transform.position += currentForce  * Time.deltaTime;
            //			rbody.AddForce(finalForce* 20 );
			if (totalForce.magnitude == 0) { 
				currentForce = currentForce * 0.1f;
			}
            

		}
        private void LateUpdate()
        {
        }
        private void OnCollisionEnter(Collision collision)
        {

            
            
            Vector3 auxPosition = collision.contacts[0].normal * -collision.contacts[0].separation;
            auxPosition.y = 0;
            transform.position += auxPosition;
        }
        public void OnCollisionStay(Collision collision)
        {
            Vector3 auxPosition = collision.contacts[0].normal * -collision.contacts[0].separation;
            auxPosition.y = 0;
            transform.position += auxPosition;
        }
        private void OnTriggerEnter(Collider other)
        {
            
        }
    }
}