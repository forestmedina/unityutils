﻿using System;
using System.Collections;
using System.Collections.Generic;

public class EnumUtils
{
	public static IEnumerable<T> Getvalues<T>(){
		foreach (object o in Enum.GetValues (typeof(T))) {
			yield return (T)o;
		}
	}
    public static T Parse<T>(string value)
    {
       
            return (T)Enum.Parse(typeof(T), value);
       
    }
    public static T ParseDefault<T>(string value,T defaultValue)
    {
        try
        {
            return Parse<T>(value);
        }
        catch
        {
            return defaultValue;
        }
        
    }
    public static string Name<T>(T value)
    {
        return Enum.GetName(typeof(T), value);
    }
}


