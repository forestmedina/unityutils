﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class Pool : MonoBehaviour {
	[SerializeField]List<GameObject> editorList;
	[SerializeField]List<GameObject> reservedList;
	[SerializeField]List<GameObject> spawneddList;
	[SerializeField]List<GameObject> instancedList;
	public GameObject prefab;
	public int maxInstances=10;
	// Use this for initialization
	private void Start () {
		reservedList = new List<GameObject> ();
		reservedList.AddRange (editorList);
		spawneddList = new List<GameObject> ();
		instancedList = new List<GameObject> ();
	}
	public void Recycle(GameObject instance){
		if (reservedList.Contains(instance)) return;
		
		reservedList.Add (instance);
		spawneddList.Remove (instance);
		instance.SetActive (false);
		instance.SetParentClearPosition (transform);


	}
    public bool WasRecycled(GameObject go)
    {
        return reservedList.Contains(go);
    }
    public GameObject Spawn(Transform copyTransform = null)
    {
        return Spawn(copyTransform, Vector3.zero);
    }
	
	// ReSharper disable once MemberCanBePrivate.Global
	public GameObject Spawn(Transform copyTransform,Vector3 offset,bool copy_position=true,bool copy_rotation=true,bool copy_scale=true)
	{
		GameObject instance;
		if (reservedList.Count > 0)
		{
			instance = reservedList[0];
			reservedList.RemoveAt(0);
		}
		else
		{
			instance = Instantiate (prefab);
			instancedList.Add (instance);
		}

		if (copyTransform != null)
            {
	            if (copy_position) instance.transform.position = copyTransform.position+offset;
	            if (copy_scale)    instance.transform.localScale= copyTransform.localScale;
	            if (copy_rotation) instance.transform.rotation= copyTransform.rotation;
            }
			
			instance.transform.parent = null;
			spawneddList.Add (instance);
			instance.SetActive (true);
			
            instance.SendMessage("OnSpawn",this,SendMessageOptions.DontRequireReceiver);
		return instance;
	}
	public void RecycleAll(){
		foreach (var go in spawneddList) {
			if (!reservedList.Contains (go)) {
				reservedList.Add (go);
				Debug.Log ("Recycling");
			}
			go.SetActive (false);
			go.SetParentClearPosition (transform);
		}
		spawneddList.Clear ();
	}
	public IEnumerable InstancedObjects(){
		foreach (var go in spawneddList) {
			yield return go;
		}
	}
	public int InstancedCount(){
		return spawneddList.Count;
	}
	public IEnumerable AllObjects(){
		foreach (var go in spawneddList) {
			yield return go;
		}
		foreach (var go in reservedList) {
			yield return go;
		}
	}
	// Update is called once per frame
	void Update () {
		
	}
}
