﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum PoolTypes{
	BasketBall,
	StringButton
}
public enum UtilityObjects{
	Health
}
[Serializable]
public class DescriptorPrefabType<T> {
    public T type;
    public GameObject prefab;
    public int maxInstances;
    public DescriptorPrefabType(){}
    public DescriptorPrefabType(T type, GameObject prefab,int maxInstances){
        this.type = type;
        this.prefab = prefab;
        this.maxInstances = maxInstances;
    }
}


public class PoolManager<T> : MonoBehaviour {
	[SerializeField]GameObject poolPrefab;
	Dictionary<T,Pool> objectPool;
	private List<Pool> poolsList;

	//[SerializeField]List<Pool> poolsEditor;
    protected virtual  IEnumerable<DescriptorPrefabType<T>> getPrefabsDescriptors()
    {
        yield return new DescriptorPrefabType<T>();
    }
    // Use this for initialization
    void Awake () {
		objectPool = new Dictionary<T, Pool> ();
	    poolsList=new List<Pool>();
		foreach (DescriptorPrefabType<T> prefabtype in getPrefabsDescriptors()) {
			GameObject poolInstance = GameObject.Instantiate (poolPrefab.gameObject);
			poolInstance.GetComponent<Pool> ().maxInstances = prefabtype.maxInstances;
			poolInstance.SetParentClearPosition (transform);
			objectPool.Add(prefabtype.type,poolInstance.GetComponent<Pool>());
			poolInstance.GetComponent<Pool> ().prefab = prefabtype.prefab;
			poolsList.Add(poolInstance.GetComponent<Pool>());
			//poolsEditor.Add (poolInstance.GetComponent<Pool> ());
		}
		
	}
	public Pool getPool(T type){
		return objectPool[type];
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}


