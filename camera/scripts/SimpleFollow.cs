﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sanuma.Camera
{
	public class SimpleFollow : MonoBehaviour
	{
		
		[SerializeField] Vector3 offset;
		[SerializeField] GameObject followed;
		float basespeed=0.4f;
		float speed;
		// Use this for initialization
		void Start ()
		{
			
		

		}
	
		// Update is called once per frame
		void Update ()
		{
			
			if (followed != null) {
				Vector3 dest = followed.transform.position+offset;
				float distance=Vector3.Distance (dest, transform.position);
				if(distance<0.1f){
					transform.position = dest;
				}else{
					speed = basespeed*0.1f +Mathf.Clamp(distance/2.0f,0,1)*basespeed*0.9f;
					Vector3 dir = (dest - transform.position).normalized;
					transform.position += dir*speed;
				}

			}
		
		}
	}

}